package game

import (
	"fmt"
	"hangman/user"
)

// Run executes the main game loop
func Run(game *Game) {
	game.ShowState()
	guess := user.AskInput()
	game.MakeGuess(guess)
	if game.UserLost() {
		fmt.Println("LOSER! 👹👹👹")
		return
	} else if game.UserWon() {
		fmt.Println("Well done! 🎉🎉🎉")
		return
	}
	Run(game)
}
