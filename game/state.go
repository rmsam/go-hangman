package game

import "fmt"

// Game state
type Game struct {
	maxAttempts       int
	successfulGuesses []rune
	failedGuesses     []rune
	secretWord        string
}

// New it's obvious what it does, right?
func New(secretWord string, maxAttempts int) Game {
	return Game{
		maxAttempts:       maxAttempts,
		successfulGuesses: make([]rune, 0),
		failedGuesses:     make([]rune, 0),
		secretWord:        secretWord,
	}
}

// MakeGuess makes a guess (surprise!!!)
func (game *Game) MakeGuess(guess rune) bool {
	if game.userGuessed(guess) {
		return true
	}
	for _, char := range game.secretWord {
		if char == guess {
			game.successfulGuesses = append(game.successfulGuesses, guess)
			return true
		}
	}
	game.failedGuesses = append(game.failedGuesses, guess)
	return false
}

// ShowState shows the state of the game
func (game Game) ShowState() {
	fmt.Println("\n==========")
	fmt.Printf("- Correct: %s\n", string(game.successfulGuesses))
	fmt.Printf("- Wrong: %s\n", string(game.failedGuesses))
	fmt.Printf("- Attempts left: %d\n", game.attemptsLeft())
}

// UserWon checks if user won
func (game Game) UserWon() bool {
	for _, char := range game.secretWord {
		if !game.userGuessed(char) {
			return false
		}
	}
	return true
}

// UserLost checks whether the user has lost
func (game Game) UserLost() bool {
	return game.attemptsLeft() == 0
}

func (game Game) attemptsLeft() int {
	return game.maxAttempts - len(game.failedGuesses)
}

func (game Game) userGuessed(char rune) bool {
	for _, guessed := range game.successfulGuesses {
		if guessed == char {
			return true
		}
	}
	return false
}
