package game

import "testing"

func TestHello(t *testing.T) {
	words := []string{"word"}
	selected := SelectWord(words)

	if selected != words[0] {
		t.Errorf("Word was incorrect, got: %s, want: %s.", selected, words[0])
	}
}
