package game

import (
	"math/rand"
	"time"
)

// SelectWord selects a word from the provided list
func SelectWord(words []string) string {
	// https://golang.cafe/blog/golang-random-number-generator.html
	rand.Seed(time.Now().UnixNano())
	var max = len(words)
	var index = rand.Intn(max)
	return words[index]
}
