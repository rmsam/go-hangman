package user

import (
	"bufio"
	"fmt"
	"os"
)

// AskInput asks for a user guess
func AskInput() rune {
	fmt.Print("Guess: ")
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	if len(text) != 2 { // 2 = char + \n
		fmt.Println("Only one char pls!")
		return AskInput()
	}
	return rune(text[0])
}
