package main

import (
	"hangman/game"
)

func main() {
	words := [3]string{"waffle", "cookie", "biscuit"}
	secretWord := game.SelectWord(words[:]) // words[:] => array to slice
	gameState := game.New(secretWord, 3)
	game.Run(&gameState)
}
